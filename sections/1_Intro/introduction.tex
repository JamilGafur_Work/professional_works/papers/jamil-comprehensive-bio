\section{Introduction}

\subsection{What are Enhancers?}

In the realm of human genomics, an intricate interplay of essential regulatory components harmonizes to direct complex biological processes. Among these vital elements, enhancers stand out as segments of DNA that possess the remarkable capacity to amplify gene activity \cite{szutorisz2005role}. Figure~\ref{fig:DNAtochromatine} visually illustrates the formation of enhancers from a given DNA sequence. Enzymes known as RNA polymerases are responsible for transcribing DNA sequences into RNA sequences. These polymerases initiate the transcription process by binding to a gene's promoter on the DNA strand. Proteins called activators play a crucial role by binding to enhancer sequences located distantly from the promoter. This binding induces looping of the DNA, bringing these distant regions into close proximity. This conformational arrangement facilitates the recruitment of RNA polymerase to the promoter, thereby initiating the transcription process. Additionally, activators have the capacity to modify the structure of chromatin, rendering the DNA more accessible for transcription. Through their interaction with enhancer sequences, activators augment the efficiency of transcription initiation by assisting in the assembly of the transcriptional machinery at the promoter site.


\subsection{Enhancer Data Quality and Dimensionality}

Enhancers exhibit a distinct ability to trigger gene transcription across significant genomic spans, thus wielding a pivotal role in shaping diverse cell types and intricate regulatory networks within multicellular organisms \cite{calo2013modification, szutorisz2005role}. At the core of genome functionality lies the regulation of gene activity, a foundational mechanism that governs a myriad of biological processes \cite{zentner2012chromatin}. This regulatory foundation pivots on the interaction between various regulatory elements and their target genes. Among these elements, enhancers emerge as key orchestrators in the regulatory landscape. These non-coding DNA segments function as potent gene activity amplifiers, exquisitely refining transcription levels and thereby exerting influence over protein expression patterns. It is approximated that enhancers may encompass as much as 10 percent of the human genome \cite{bulger2011functional}.

As integral constituents of this intricate regulatory network, enhancers exercise precision in modulating transcriptional levels \cite{zentner2012chromatin}. This modulation orchestrates cellular functions, culminating in distinct cell lineages, each characterized by a distinctive gene expression profile. Such meticulous control establishes cellular identity and function, thus contributing to the remarkable diversity and specialization of cell types within multicellular organisms.

The deciphering of enhancer functionality fundamentally hinges on a profound comprehension of the intricate web of regulatory interactions that govern gene networks and their associated expression levels. Enhancers, occupying a central role within these interactions, serve as mediators of both spatial and temporal dimensions of gene regulation. A more profound understanding of these regulatory intricacies, as facilitated by the dynamic interplay with enhancers, provides profound insights into the molecular underpinnings that propel various cellular processes.

Historically, the prediction of enhancers heavily leaned upon DNA sequence analysis \cite{srivastava2021interpretable, kumar2016predicting}. Nevertheless, the current study deviates from conventional methodologies by introducing an innovative approach. This departure not only opens up novel perspectives but also harbors the potential for groundbreaking advancements in enhancer prediction, thereby propelling our comprehension of intricate regulatory networks within the realm of human genomics.

\begin{figure}
\centering
\includegraphics[width=0.8\textwidth]{./images/DNA_to_Enhancer.png}
\caption{Illustration depicting the progression from DNA sequence to enhancer. The upper panel represents a DNA sequence, the middle panel portrays the orchestrated action of Transcription Factors, RNA Polymerase, and Activators, while the lower panel highlights an RNA sequence bonded as enhancers.}
\label{fig:DNAtochromatine}
\end{figure}


\subsection{Enhancers in Machine Learning}

The fusion of sequencing data with machine learning models represents a transformative approach to unravel the intricacies of gene regulation and cellular identity. Computational frameworks empowered by artificial intelligence uncover patterns within extensive sequencing datasets, revealing the underlying regulatory grammar governing biological processes. Integrating sequencing data with ML algorithms holds the potential to unveil hidden insights and predictive capabilities, thereby reshaping biological exploration. In recent times, machine learning models trained on epigenomics data have demonstrated significant efficacy in predicting enhancers and identifying binding sites for transcription factors (TFs), including those genetic variants impacting chromatin accessibility \cite{kelley2016basset, patel2021global, chen2022sequence}.

% Subsequent sections delve into various aspects of this interdisciplinary landscape:

% \begin{enumerate}
% \item Section \ref{sec:cell_lines} explores the significance of cell lines in biological research.
% \item Section \ref{sec:ENCODE} discusses the ENCODE system.
% \item Sections \ref{sec:ChIP-seq}, \ref{sec:ATAC-seq}, and \ref{sec:STARR-seq} introduce the ChIP-seq, ATAC-seq, and STARR-seq techniques, respectively.
% \item Section \ref{sec:chromatin_data} discusses the role of chromatin in gene regulation.
% \item Section \ref{sec:TFE} explores the foundational principles and quantification paradigms of different sequencing techniques, shedding light on their capabilities and limitations.
% \item Section \ref{sec:ML} introduces current Machine Learning algorithms, their diverse architectures, and utilization workflows.
% \item Section \ref{sec:CEA} discusses the role of Machine Learning in Chromatin Enhancer Analysis.
% \item Sections \ref{sec:CEA}, \ref{sec:DNA}, \ref{sec:Rationale}, and \ref{sec:validation} introduce the role of Machine Learning in Chromatin Enhancer Analysis, Diverse Network Architectures, Rationale for the Endeavor, and Validation Techniques, respectively.
% \item Section \ref{sec:openproblem} and \ref{sec:SHAP} delve into current open problems and the SHAP framework, respectively.
% \item Lastly, Section \ref{sec:conclusion} draws our discussion to a conclusion.
% \end{enumerate}


% https://www.google.com/imgres?imgurl=https%3A%2F%2Fonlinelibrary.wiley.com%2Fcms%2Fasset%2F599a9d09-ad18-4c30-87a8-07e80fb0e426%2Fggn2202200015-fig-0001-m.jpg&tbnid=VLbw1BHE4FpU_M&vet=12ahUKEwj9qvf194aBAxWqMN4AHdDEDW8QMygUegQIARB2..i&imgrefurl=https%3A%2F%2Fonlinelibrary.wiley.com%2Fdoi%2Ffull%2F10.1002%2Fggn2.202200015&docid=B13iWMaHJuYEZM&w=1000&h=390&q=Pioneer%20factors%2C%20sequence-specific%20TFs%2C%20and%20general%20transcription%20factors.&ved=2ahUKEwj9qvf194aBAxWqMN4AHdDEDW8QMygUegQIARB2