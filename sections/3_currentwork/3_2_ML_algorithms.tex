
\section{Machine Learning Algorithms}
\label{sec:ML}

Machine Learning (ML) algorithms have played a pivotal role in the genomic revolution, enabling the deciphering of intricate patterns within genomes. These algorithms form the cornerstone of genomic investigation, allowing researchers to sift through voluminous data, revealing significant genetic variations and associations critical for medical breakthroughs and personalized interventions.

\subsection{Neural Networks}

Neural Networks have found extensive application in genomics, particularly in gene expression analysis, owing to their capacity to learn intricate non-linear relationships between input and output variables, thus serving as a potent tool for gene expression analysis.

\subsection{Dense Neural Networks}

Dense Neural Networks (DNNs), also referred to as fully connected neural networks or multi-layer perceptrons, are fundamental architectural frameworks within artificial neural networks. A DNN is characterized by interconnected neuron layers, where each neuron in a given layer is intricately linked to every neuron in the subsequent layer. This intricate interconnection results in a dense matrix of weighted connections, leading to the term "dense." DNNs possess an aptitude for modeling complex data relationships, rendering them suitable for tasks such as pattern recognition, classification, regression, and feature extraction.

At its core, a DNN consists of hierarchical layers, including an input layer, one or more hidden layers, and an output layer. The input layer receives raw data or features, which are transformed and propagated through the network. Hidden layers progressively abstract features from the input data. Neurons within these layers compute weighted sums of inputs, which are then passed through activation functions to introduce non-linearity. This enables the network to capture intricate data relationships. The output layer synthesizes these features into outcomes, such as classifications or regressions.

The network's capacity to learn and generalize is rooted in its parameterized architecture. During training, the DNN iteratively adjusts weights and biases using optimization algorithms like gradient descent, with the aim of minimizing a predefined loss function. This process involves comparing predictions to ground truth labels and refining internal representations. As training progresses, the DNN discerns features, filters out noise, and adapts to data patterns. This adaptability empowers DNNs to encapsulate complex data distributions, allowing them to address diverse tasks across various domains.

\begin{figure}[h!]
\centering
\includegraphics[width=0.5\textwidth]{./images/Feed_Forward.png}
\caption{A Dense Neural Network (DNN) features interconnected neuron layers, forming a dense matrix of weighted connections. This architecture excels in modeling complex data relationships, suitable for various tasks like pattern recognition and classification.}
\label{fig:DNN}
\end{figure}

Although DNNs exhibit remarkable performance, they encounter challenges stemming from the proliferation of layers and neurons. This can lead to training complexities, overfitting, and heightened resource demands. Techniques such as regularization, dropout, and batch normalization have been introduced to mitigate these issues. The success of DNNs is contingent on the availability of high-quality labeled data for effective training.
\subsection{Recurrent Neural Networks}

Recurrent Neural Networks (\textbf{RNNs}) stand out in the realm of deep learning for their proficiency in handling sequential data. They achieve this by retaining and effectively utilizing information from previous time steps, distinguishing them from feedforward neural networks. The fundamental strength of RNNs lies in their capacity to maintain hidden states, enabling them to capture input context and dependencies. Consequently, RNNs find widespread application in sequential tasks, such as natural language processing, speech recognition, and time series analysis.

An essential advancement in the domain of RNNs is the emergence of \textbf{Long Short-Term Memory (LSTM)} networks. LSTMs address critical limitations associated with vanilla RNNs, most notably the vanishing gradient problem. They achieve this by introducing sophisticated gating mechanisms designed to capture long-range dependencies within sequences. The architectural components of LSTM networks include memory cells as well as input, forget, and output gates. These gates exercise precise control over the flow of information, affording LSTMs the ability to remember or discard contextual information strategically.

The operation of LSTM gates is central to the network's functionality. Let's delve into how these gates work:

\begin{itemize}
    \item \textbf{Input Gate}: The input gate determines which information from the current time step should be stored in the memory cell. It does this by applying a sigmoid activation function to the weighted sum of the current input and the previous hidden state. This gate regulates the update of the cell state by allowing relevant information to flow into it.

    \item \textbf{Forget Gate}: The forget gate decides what information from the previous cell state should be discarded. It employs a sigmoid activation function on the weighted sum of the previous hidden state and the current input. This gate is crucial for filtering out irrelevant or outdated information, preventing the cell state from becoming cluttered.

    \item \textbf{Memory Cell}: The memory cell stores and updates information over time. It is updated based on the output of the input and forget gates. This enables the LSTM to capture both short-term and long-term dependencies in the data.

    \item \textbf{Output Gate}: The output gate regulates the information that is passed to the next layer or used as the network's output. It uses a sigmoid activation function on the weighted sum of the current input and the previous hidden state, combined with the hyperbolic tangent (tanh) of the current cell state. This gate ensures that only relevant information is propagated further.

\end{itemize}

The importance of these gates in LSTM networks cannot be overstated. They allow LSTMs to selectively capture and retain information from previous time steps while filtering out noise and irrelevant data. This selective memory and information flow are crucial for effectively handling sequences with long-range dependencies, making LSTMs particularly valuable in various applications where context preservation is vital.

\begin{figure}[h!]
    \centering
    \includegraphics[width=0.5\textwidth]{./images/LSTM.png}
    \caption{Long Short-Term Memory (LSTM) networks excel in capturing long-range dependencies in sequential data, effectively overcoming the limitations of conventional Recurrent Neural Networks (RNNs). The upper row represents the cell state, and the columns depict the functionality of the gates: forget gate, input gate, memory cell, and output gate. Here, `x' represents pointwise multiplication, and `+' signifies pointwise addition.}
    \label{fig:LSTM}
\end{figure}

The clear distinction and controlled operation of these gates enable LSTMs to mitigate the vanishing gradient problem encountered in standard RNNs, allowing them to excel in modeling complex sequential data.


\subsection{Convolutional Neural Networks}

Convolutional Neural Networks (CNNs) have emerged as a transformative force in the realm of deep learning, particularly tailored for the intricate analysis of visual data, encompassing images and videos. Inspired by the intricate workings of the human visual system, CNNs have showcased exceptional prowess in tasks such as image recognition, classification, and feature extraction. This distinctive architectural design empowers CNNs to glean hierarchical patterns directly from raw pixel data, circumventing the challenges often encountered by conventional machine learning paradigms.

At the heart of CNNs lies a suite of specialized layers that collectively constitute a potent computational framework. Among these layers, convolutional layers assume a pivotal role, harnessing filters to systematically traverse input data, thereby adeptly extracting features and discerning spatial relationships. These filters exhibit an innate capacity to identify rudimentary features in the lower strata of the network and progressively assemble more intricate features as the network's depth increases.

Consider an input matrix $I$, which faithfully represents a grayscale image patch:

\[
I =
\begin{bmatrix}
1 & 2 & 3 \\
0 & 1 & 2 \\
2 & 3 & 4 \\
\end{bmatrix}
\]

Now, let us define a filter (often referred to as a kernel) denoted as $K$:

\[
K =
\begin{bmatrix}
1 & 0 \\
-1 & 1 \\
\end{bmatrix}
\]

To execute a 2D convolution operation between $I$ and $K$, a meticulous procedure unfolds. The filter is meticulously slid across the input matrix, with an element-wise dot product computed at each position. These individual results are then amalgamated to produce the output matrix $O$. Importantly, a stride of 1 is set, and no padding is introduced:

\[
O_{i,j} = \sum_{m}\sum_{n} I_{i+m,j+n} \cdot K_{m,n}
\]

As an illustrative example, let's calculate a specific element of the output matrix, $O_{1,1}$:

\[
O_{1,1} = (1 \cdot 1) + (2 \cdot 0) + (3 \cdot -1) + (0 \cdot 1) + (1 \cdot 0) + (2 \cdot 1) + (2 \cdot -1) + (3 \cdot 1) + (4 \cdot 0) = -3
\]

By diligently executing this computation for all elements in the output matrix, we derive the final result:

\[
O =
\begin{bmatrix}
-3 & -3 \\
0 & 3 \\
-2 & 5 \\
\end{bmatrix}
\]

The hierarchical architecture of CNNs is further enriched through the incorporation of pooling layers. These layers, assuming a dual role, facilitate data down-sampling while concurrently preserving critical features. Subsequently, fully connected layers come into play, aggregating these features to enable high-level decision-making processes.

The training regimen of CNNs revolves around the optimization of parameters, a task accomplished through techniques such as backpropagation and gradient descent. During the training phase, the network's filters evolve to specialize in recognizing specific features, constantly honing their representations. Notably, the availability of labeled training data emerges as a pivotal factor in enhancing the robustness and generalization capabilities of CNNs, thus enabling their effective deployment across a diverse array of real-world applications.

\section{Machine Learning Workflow}

The process of developing machine learning models for enhancer prediction follows a structured workflow encompassing training, validation, and testing stages, as illustrated in Figure~\ref{fig:training_loop}. Each stage plays a pivotal role in creating robust and accurate models. This section outlines the essential components and equations associated with each phase.

\subsection{Training}

The training phase forms the cornerstone of model development, during which the model acquires knowledge from labeled data. In the context of enhancer prediction, this entails exposing the model to a dataset comprising known enhancer sequences and non-enhancer sequences. The primary objective is for the model to discern the intricate patterns and features that distinguish enhancers from other genomic regions.

The training process involves the iterative adjustment of the model's parameters, encompassing weights and biases, to minimize a predefined loss function. This optimization is achieved through well-established algorithms like gradient descent. The weight update equation, referenced as Equation~\ref{eq:weight_update}, governs this parameter refinement:

\begin{equation}\label{eq:weight_update}
\mathbf{W}{\text{new}} = \mathbf{W}{\text{old}} - \alpha \cdot \frac{\partial \mathcal{L}}{\partial \mathbf{W}}
\end{equation}

Where:
\begin{enumerate}
\item $\mathbf{W}{\text{new}}$ represents the updated weight matrix.
\item $\mathbf{W}{\text{old}}$ denotes the current weight matrix.
\item $\alpha$ signifies the learning rate, serving as the control for step size in weight updates.
\item $\mathcal{L}$ stands for the loss function, quantifying the dissimilarity between predicted and actual labels.
\item $\frac{\partial \mathcal{L}}{\partial \mathbf{W}}$ represents the gradient of the loss concerning the model's weights.
\end{enumerate}

Throughout the training process, the model adapts its internal parameters to capture pertinent features while filtering out noise from the data. This adaptability empowers the model to generalize effectively and make accurate predictions on unseen data.

\subsection{Validation}

Validation assumes a critical role in assessing a model's performance during the training phase. It entails evaluating the model on a distinct validation dataset that has not been part of the training process. The validation step safeguards against overfitting, ensuring that the model can generalize effectively to new instances.

A common practice in validation is early stopping, where training halts if the model's performance on the validation dataset starts to degrade, signaling potential overfitting. Additionally, validation aids in hyperparameter tuning, enabling the selection of optimal model configurations.

\subsection{Testing}

The testing phase serves as the final evaluation of the model's generalization capabilities. It assesses the model's performance on an independent test dataset, entirely separate from both the training and validation sets. During this phase, various performance metrics, including accuracy, precision, recall, AUROC (Area Under the Receiver Operating Characteristic curve), and AUPRC (Area Under the Precision-Recall curve), are computed to gauge the model's effectiveness. This rigorous evaluation ensures that the model is well-prepared for deployment in practical applications.

\begin{figure}[h!]
\centering
\includegraphics[width=0.8\textwidth]{./images/training_neural_network.png}
\caption{Training Neural Network Loop}
\label{fig:training_loop}
\end{figure}

The workflow, encompassing training, validation, and testing, forms the cornerstone of developing robust and accurate machine learning models for enhancer prediction. These meticulous steps ensure that the model is well-equipped to make precise predictions in real-world scenarios.